import 'package:flutter/material.dart';
import 'package:sausage_app/screens/challenges_screen.dart';
import 'screens/MyHomePage.dart';


void main() {
  runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sausage Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // home: MyHomePage(title: 'Sausage App'),
      home: ChallengesList(title: 'Challenges'),
      routes: {
        MyHomePage.routeName: (context) => MyHomePage(title: 'Sausage App', currency: ' ',),
      },
    );
  }
}

class Constants {
  // ignore: non_constant_identifier_names
  static final String user_story_1 = 'As a Greggs Fanatic '
      'I want to be able to get the latest menu of products rather than the random static products it returns now'
      'So that I get the most recently available products.';
  // ignore: non_constant_identifier_names
  static final String user_story_2 = 'As a Greggs Entrepreneur '
      'I want to get the price of the products returned to me in Euros'
      'So that I can set up a shop in Europe as part of our expansion';
  static final String currencyEU = '€';
  static final String currencyUK = '£';
  static final double exchange = 1.11;
}

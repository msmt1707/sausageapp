import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sausage_app/Constants.dart';
import 'package:sausage_app/models/sausage_model.dart';
import 'dart:convert';

class MyHomePage extends StatefulWidget {
  static final routeName = '/myhomepage';
  MyHomePage({Key key, this.title, this.currency}) : super(key: key);
  final String title;
  final String currency;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List sausages = const [];

  Future loadSausages() async {
    String content = await rootBundle.loadString('assets/data/sausage.json');
    List collection = json.decode(content);
    List<SausageModel> _sausages =
        collection.map((el) => SausageModel.fromJSON(el)).toList();

    setState(() {
      sausages = _sausages;
      print('non ordered');
      sausages.forEach((sausages) => print(sausages.date));
    });
  }

  void initState() {
    loadSausages();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text(widget.title)),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: ListView.builder(
            itemCount: sausages.length,
            itemBuilder: (context, index) {
              SausageModel sausage = sausages[index];
              return Card(
                margin: EdgeInsets.all(15),
                child: ListTile(
                  title: Text(sausage.name),
                  subtitle: Text(sausage.date),
                  trailing:
                      Text('${widget.currency}  ${sausage.price}'),
                ),
              );
            }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {
          if (widget.currency == Constants.currencyUK)
            {
              setState(() {
                sausages.sort((a, b) => b.date.compareTo(a.date));
              })
            }
          else
            {
              setState(() {
                // TODO: restrict to convert only once.
                String el = '';
                sausages.forEach(
                  (sausages) {
                    sausages.price *= Constants.exchange;
                    sausages.price.toString();
                    el = sausages.price.toStringAsFixed(2);
                    sausages.price = double.parse(el);
                  },
                );
                print('changed');
                sausages.forEach((sausages) => print(sausages.price));
              })
            }
        },
        tooltip: 'Make Change',
        child: widget.currency == Constants.currencyUK
            ? Icon(Icons.low_priority)
            : Text(Constants.currencyEU),
        mini: true,
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

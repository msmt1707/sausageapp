import 'package:flutter/material.dart';
import 'package:sausage_app/Constants.dart';
import 'package:sausage_app/screens/MyHomePage.dart';

class ChallengesList extends StatelessWidget {
  static final routeName = '/challenges_screen';
  ChallengesList({Key key, this.title}) : super(key: key);
  final String title;

  void _nextScreen(BuildContext context, String title, String currency) {
    Navigator.push(context, MaterialPageRoute( builder: (context) => MyHomePage(title: title, currency: currency,)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(title),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(40.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'User story 1',
                style: TextStyle(fontSize: 15),
              ),
              Text(
                Constants.user_story_1 == null ? ' ' : Constants.user_story_1,
                maxLines: 5,
                style: TextStyle(fontSize: 15),
              ),
              ElevatedButton(
                onPressed: () => {
                  _nextScreen(context, 'Sausage App - Sorted', Constants.currencyUK)
                },
                child: Text(
                  'User story 1',
                ),
              ),
              SizedBox(
                height: 25,
              ),
              Text(
                'User story 2',
                maxLines: 5,
                overflow: TextOverflow.clip,
                style: TextStyle(fontSize: 15),
              ),
              Text(
                Constants.user_story_2 == null ? ' ' : Constants.user_story_2,
                maxLines: 5,
                style: TextStyle(fontSize: 15),
              ),
              ElevatedButton(
                onPressed: () => {_nextScreen(context, 'Sausage App - Euros', '€')},
                child: Text(
                  'User story 2',
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

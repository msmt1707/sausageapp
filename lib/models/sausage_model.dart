
class SausageModel{
  String name;
  double price;
  String date;

  SausageModel({this.name, this.price, this.date});

  SausageModel.fromJSON(Map<String, dynamic> parsedJson) {
    name = parsedJson['name'];
    price = parsedJson['price'];
    date = parsedJson['date'];
  }
}
